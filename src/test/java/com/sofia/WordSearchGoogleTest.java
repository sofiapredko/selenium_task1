package com.sofia;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.assertTrue;

public class WordSearchGoogleTest {
    private static final Logger LOG = LogManager.getLogger(WordSearchGoogleTest.class);
    private static final String TEST_BASE_PAGE = "https://google.com";
    private static final String DRIVER_PATH = "src/main/resources/chromedriver.exe";
    private static final String WEB_DRIVER_NAME = "webdriver.chrome.driver";

    private static WebDriver driver;

    static {
        System.setProperty(WEB_DRIVER_NAME, DRIVER_PATH);
        driver = new ChromeDriver();
    }

    @BeforeTest
    public void setObjects() {
        driver.manage().timeouts()
                .implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(TEST_BASE_PAGE);
    }

    @Test
    public void searchWordResults() {
        WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("Apple");
        element.submit();
        LOG.info("Title: " + driver.getTitle());
        Assert.assertTrue(driver.getTitle().toLowerCase().contains("apple"));
        driver.findElement(By.cssSelector("a.q.qs")).click();
        WebElement imagePageElement = driver.findElement(By.cssSelector("div.hdtb-mitem.hdtb-msel.hdtb-imb"));
        assertTrue(imagePageElement.getAttribute("aria-selected"), true);
    }

    @AfterTest
    public void endTest() {
        driver.quit();
    }

}
